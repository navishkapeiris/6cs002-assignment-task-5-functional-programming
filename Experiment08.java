package Experiments;
import java.util.*;

public class Experiment08 {

  public void run() {
    // Stock market data example
    String[] stockData = {
"AAPL, Apple Inc., $150.5, 100000",
"GOOGL, Alphabet Inc., $2500.75, 50000",
"AMZN, Amazon.com Inc., $3200.4, 75000",
"MSFT, Microsoft Corp., $300.2, 80000",
"TSLA, Tesla Inc., $800.6, 120000",
"FB, Meta Platforms, $340.9, 60000",
"NFLX, Netflix Inc., $600.3, 90000",
"GOOG, Alphabet Inc., $2800.1, 70000",
"NVDA, NVIDIA Corp., $750.25, 55000",
"BABA, Alibaba Group, $220.75, 110000",
"IBM, IBM Corp., $140.3, 65000",
"INTC, Intel Corp., $50.75, 95000",
"ORCL, Oracle Corp., $80.2, 85000",
"V, Visa Inc., $220.6, 78000",
"PYPL, PayPal Holdings, $300.8, 88000",
"GS, Goldman Sachs, $400.5, 72000",
"JPM, JPMorgan Chase, $150.75, 105000",
"MS, Morgan Stanley, $85.9, 68000"
    };

    List<String> stockList = Arrays.asList(stockData);  

    System.out.println("Serial stock data\n---------");
    stockList.stream().forEach(stockEntry -> System.out.println(stockEntry));

    System.out.println("\nParallel stock data\n---------");
    stockList.parallelStream().forEach(stockEntry -> System.out.println(stockEntry));
  }

  public static void main(String[] args) {
    new Experiment08().run();
  }
}
