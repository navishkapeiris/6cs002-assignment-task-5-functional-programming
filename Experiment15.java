package Experiments;
import java.util.function.Function;
import java.util.stream.IntStream;

public class Experiment15 {
  public static void main(String[] args) {
    Function<String, StockMarketEntry> parseStockEntry = entry -> {
      String[] parts = entry.split(", ");
      String symbol = parts[0];
      String name = parts[1];
      double price = Double.parseDouble(parts[2].substring(1)); // Removing the dollar sign
      return new StockMarketEntry(symbol, name, price);
    };

    String stockEntryString = "AAPL, Apple Inc., $150.25";
    StockMarketEntry parsedEntry = parseStockEntry.apply(stockEntryString);
    System.out.println(parsedEntry);

    IntStream range = IntStream.range(1, 10);
    range.mapToObj(i -> parseStockEntry.apply("AAPL, Apple Inc., $" + (i * 150.25))) // Assuming AAPL price increases
        .mapToDouble(StockMarketEntry::getPrice)
        .forEach(price -> System.out.println(price));
  }

  static class StockMarketEntry {
    private String symbol;
    private String name;
    private double price;

    public StockMarketEntry(String symbol, String name, double price) {
      this.symbol = symbol;
      this.name = name;
      this.price = price;
    }

    public double getPrice() {
      return price;
    }

    @Override
    public String toString() {
      return "StockMarketEntry{" +
             "symbol='" + symbol + '\'' +
             ", name='" + name + '\'' +
             ", price=" + price +
             '}';
    }
  }
}
