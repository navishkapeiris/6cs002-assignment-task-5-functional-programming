package Experiments;
import java.util.*;

public class Experiment03 {
  public static void main(String[] args) {
    // Stock market data example
    String[] stockData = {
        "AAPL, Apple Inc., $150.25",
        "GOOGL, Alphabet Inc., $2800.50",
        "MSFT, Microsoft Corporation, $310.75"
    };

    List<String> stockList = Arrays.asList(stockData);  
    System.out.println(stockList.getClass());

    System.out.println("Stock Market Data:");
    
    for(String stock : stockList) {
      System.out.println(stock);
    }
  }
}

