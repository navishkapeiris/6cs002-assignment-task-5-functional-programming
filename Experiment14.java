package Experiments;
import java.util.stream.Stream;

public class Experiment14 {
  class StockEntryStreamBuilder {
    public Stream<String> build() {
      Stream.Builder<String> builder = Stream.builder();
        builder.add("AAPL, Apple Inc., $150.25");
        builder.add("GOOGL, Alphabet Inc., $2800.50");
        builder.add("MSFT, Microsoft Corporation, $310.75");
        builder.add("NFLX, Netflix Inc., 600.3, 90000");
        builder.add("GOOG, Alphabet Inc., 2800.1, 70000");
        builder.add("NVDA, NVIDIA Corp, 750.25, 55000");
        builder.add("BABA, Alibaba Group, 220.75, 110000");
        builder.add("IBM, IBM Corp, 140.3, 65000");
        builder.add("INTC, Intel Corp, 50.75, 95000");
        builder.add("AMD, Advanced Micro, 110.8, 60000");
        builder.add("INTEL, Intel Corporation, 47.9, 88000");
        builder.add("Cisco, Cisco Systems, 60.2, 70000");
        builder.add("PFE, Pfizer Inc, 42.75, 95000");
        builder.add("MRNA, Moderna Inc, 240.3, 110000");
      return builder.build();
    }
  }

  public void run() {
    StockEntryStreamBuilder builder = new StockEntryStreamBuilder();
    Stream<String> stockEntryStream = builder.build();
    stockEntryStream.forEach(entry -> System.out.println(entry));
  }

  public static void main(String[] args) {
    new Experiment14().run();
  }
}
