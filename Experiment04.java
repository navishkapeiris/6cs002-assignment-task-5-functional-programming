package Experiments;
import java.lang.reflect.Method;
import java.util.*;

public class Experiment04 {
  public static void main(String[] args) {
    // Stock market data example
    String[] stockData = {
        "AAPL, Apple Inc., $150.25",
        "GOOGL, Alphabet Inc., $2800.50",
        "MSFT, Microsoft Corporation, $310.75"
    };

    List<String> stockList = Arrays.asList(stockData);  
    System.out.println(stockList.getClass());

    System.out.println("Methods available for an immutable list:");
    
    for(Method m: stockList.getClass().getDeclaredMethods()) {
      System.out.println(m.getName());
    }
    
    // Uncommenting the line below will result in a compilation error
    // stockList.add("TSLA, Tesla Inc., $900.00");
  }
}
