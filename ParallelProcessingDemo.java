package StockMarcket;
import java.util.Arrays;
import java.util.List;

public class ParallelProcessingDemo {

    public static void main(String[] args) {
        // Create a list of stock objects
        List<Stock> stockList = Arrays.asList(
                new Stock("AAPL", "Apple Inc.", 150.50),
                new Stock("GOOGL", "Alphabet Inc.", 1200.75)
        );

        // Regular stream processing
        long startTimeRegular = System.currentTimeMillis();
        double totalValueRegular = stockList.stream()
                .mapToDouble(Stock::getPrice)
                .sum();
        long endTimeRegular = System.currentTimeMillis();
        System.out.println("Regular Stream Total Value: $" + totalValueRegular);
        System.out.println("Regular Stream Processing Time: " + (endTimeRegular - startTimeRegular) + " ms");

        // Parallel stream processing
        long startTimeParallel = System.currentTimeMillis();
        double totalValueParallel = stockList.parallelStream()
                .mapToDouble(Stock::getPrice)
                .sum();
        long endTimeParallel = System.currentTimeMillis();
        System.out.println("Parallel Stream Total Value: $" + totalValueParallel);
        System.out.println("Parallel Stream Processing Time: " + (endTimeParallel - startTimeParallel) + " ms");
    }

    // Stock class with a simplified structure
    static class Stock {
        private String symbol;
        private String name;
        private double price;

        public Stock(String symbol, String name, double price) {
            this.symbol = symbol;
            this.name = name;
            this.price = price;
        }

        public double getPrice() {
            return price;
        }
    }
}

