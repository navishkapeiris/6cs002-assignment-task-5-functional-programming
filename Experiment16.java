package Experiments;
import java.time.LocalDate;
import java.util.function.Supplier;
public class Experiment16 {
  public static void main(String[] args) {
    Supplier<StockMarketData> stockDataSupplier = new Supplier<StockMarketData>() {
      public StockMarketData get() {
        // Assuming StockMarketData is a class with appropriate fields (symbol, price, timestamp, etc.)
        String symbol = "AAPL";
        double price = 150.25;
        LocalDate timestamp = LocalDate.now();
        return new StockMarketData(symbol, price, timestamp);
      }
    };

    StockMarketData stockData = stockDataSupplier.get();
    System.out.println("Stock Data: " + stockData);
  }

  static class StockMarketData {
    private String symbol;
    private double price;
    private LocalDate timestamp;

    public StockMarketData(String symbol, double price, LocalDate timestamp) {
      this.symbol = symbol;
      this.price = price;
      this.timestamp = timestamp;
    }

    @Override
    public String toString() {
      return "StockMarketData{" +
             "symbol='" + symbol + '\'' +
             ", price=" + price +
             ", timestamp=" + timestamp +
             '}';
    }
  }
}
