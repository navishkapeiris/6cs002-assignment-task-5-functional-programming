package StockMarcket;
import java.util.Arrays;
import java.util.List;

public class StockMarket04 {
    public static void main(String[] args) {
        List<Stock> stockList = Arrays.asList(
                new Stock("AAPL", "Apple Inc.", 150.50, 155.25, 1000000, 154.75, 0.50, 1.0),
                new Stock("GOOGL", "Alphabet Inc.", 1200.75, 1210.50, 800000, 1208.20, 0.75, 1.0)
                
        );

        double targetPrice = 1200.75; 

        System.out.println("Stocks with price $" + targetPrice + ":");
        stockList.stream().filter(stock -> stock.getClosePrice() == targetPrice)
                .forEach(System.out::println);

        System.out.println();
        System.out.println("Stocks with price $1300.00:");
        stockList.stream().filter(stock -> stock.getClosePrice() == 1300.00)
                .forEach(System.out::println);
    }
}
