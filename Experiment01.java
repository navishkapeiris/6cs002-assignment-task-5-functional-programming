package Experiments;
public class Experiment01 {
	  public static void main(String[] args) {
	    // Stock market data example
	    String[] stockData = {
	        "AAPL, Apple Inc., $150.25",
	        "GOOGL, Alphabet Inc., $2800.50",
	        "MSFT, Microsoft Corporation, $310.75"
	    };

	    System.out.println("Stock Market Data:");
	    
	    for(int i = 0; i < stockData.length; i++) {
	      System.out.println(stockData[i]);
	    }
	  }
	}
