package Experiments;
import java.util.*;
import java.util.function.Predicate;

public class Experiment10 {

    // Custom Predicate for filtering stock entries based on a specific condition
    class StockFilter implements Predicate<String> {
        public boolean test(String stockEntry) {
            String[] parts = stockEntry.split(",");
            if (parts.length >= 3) {
                String stockPriceString = parts[2].trim().replace("$", "");
                try {
                    double stockPrice = Double.parseDouble(stockPriceString);
                    return stockPrice > 2000.0; // Adjust the condition based on your specific requirements
                } catch (NumberFormatException e) {
                    // Handle parsing errors, such as invalid number format
                    return false;
                }
            }
            return false; // Handle cases where the stock entry doesn't have enough parts
        }
    }

    public void run() {
        // Stock market data example
        String[] stockData = {
            "NFLX, Netflix Inc., $600.3, 90000",
            "GOOG, Alphabet Inc., $2800.1, 70000",
            "NVDA, NVIDIA Corp, $750.25, 55000",
            "BABA, Alibaba Group, $220.75, 110000",
            "IBM, IBM Corp, $140.3, 65000",
            "INTC, Intel Corp, $50.75, 95000",
            "AMD, Advanced Micro, $110.8, 60000",
            "INTEL, Intel Corporation, $47.9, 88000",
            "Cisco, Cisco Systems, $60.2, 70000",
            "PFE, Pfizer Inc, $42.75, 95000",
            "MRNA, Moderna Inc, $240.3, 110000"
        };

        List<String> stockList = Arrays.asList(stockData);

        // Filter stock entries based on the custom condition
        stockList.stream().filter(new StockFilter())
                .forEach(filteredStockEntry -> System.out.println(filteredStockEntry));
    }

    public static void main(String[] args) {
        new Experiment10().run();
    }
}
