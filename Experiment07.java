package Experiments;
import java.util.*;
import java.util.function.Consumer;
public class Experiment07 {

  public void run() {
    // Stock market data example
    String[] stockData = {
          "NFLX, Netflix Inc., 600.3, 90000",
        "GOOG, Alphabet Inc., 2800.1, 70000",
        "NVDA, NVIDIA Corp, 750.25, 55000",
        "BABA, Alibaba Group, 220.75, 110000",
        "IBM, IBM Corp, 140.3, 65000",
        "INTC, Intel Corp, 50.75, 95000"
    };

    List<String> stockList = Arrays.asList(stockData);  

    // Using forEach and an anonymous inner class to print stock data
    stockList.forEach(new Consumer<String>() {
      public void accept(String stockEntry) {
        System.out.println(stockEntry);     
      }
    });
  }

  public static void main(String[] args) {
    new Experiment07().run();
  }
}
