package Files;
import java.io.*;
public class Files05 {

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("C:\\Users\\Navishka Peiris\\Desktop\\work space\\"
    		+ "NaviApplication\\src\\Files\\stockmarket.txt"));

    r.lines().sorted((a, b) -> {
      if (a.length() == b.length())
        return 0;
      if (a.length() < b.length())
        return 1;
      return -1;
    }).forEach(l -> System.out.println(l));

    r.close();
  }

}
