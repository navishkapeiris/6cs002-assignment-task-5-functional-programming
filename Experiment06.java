package Experiments;
import java.util.*;
import java.util.function.Consumer;
public class Experiment06 {
  class StockPrintConsumer implements Consumer<String>{
    public void accept(String stockEntry) {
      System.out.println(stockEntry);     
    } 
  }
  
  public void run() {
    // Stock market data example
    String[] stockData = {
        "AAPL, Apple Inc., $150.25",
        "GOOGL, Alphabet Inc., $2800.50",
        "MSFT, Microsoft Corporation, $310.75",
        "V, Visa Inc., $220.6",
        "BAC, Bank of America, $35.25",
        "Cisco, Cisco Systems, $60.2"
    };

    List<String> stockList = Arrays.asList(stockData);  

    // Using forEach and a custom consumer to print stock data
    stockList.forEach(new StockPrintConsumer());
  }
  
  public static void main(String[] args) {
    new Experiment06().run();
  }
}
