package Files;
import java.io.*;

public class Files03 {

  public static void main(String[] args) throws Exception {
    BufferedReader r = new BufferedReader(new FileReader("C:\\Users\\Navishka Peiris\\Desktop\\work space\\"
    		+ "NaviApplication\\src\\Files\\stockmarket.txt"));

    r.lines().filter(l -> l.contains("his"))
        .forEach(l -> System.out.println(l));

    r.close();
  }

}
