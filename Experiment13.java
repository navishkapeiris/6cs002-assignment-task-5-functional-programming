package Experiments;
import java.util.function.Supplier;

public class Experiment13 {
  class StockEntrySupplier implements Supplier<String> {
    String[] stocks = {
        "NFLX, Netflix Inc., 600.3, 90000",
        "GOOG, Alphabet Inc., 2800.1, 70000",
        "NVDA, NVIDIA Corp, 750.25, 55000",
        "BABA, Alibaba Group, 220.75, 110000",
        "IBM, IBM Corp, 140.3, 65000",
        "INTC, Intel Corp, 50.75, 95000",
        "AMD, Advanced Micro, 110.8, 60000",
        "INTEL, Intel Corporation, 47.9, 88000",
        "Cisco, Cisco Systems, 60.2, 70000",
        "PFE, Pfizer Inc, 42.75, 95000",
        "MRNA, Moderna Inc, 240.3, 110000"
    };
    int nextIndex = 0;

    public String get() {
      if (nextIndex < stocks.length) {
        return stocks[nextIndex++];
      }
      return null;
    }
  }
