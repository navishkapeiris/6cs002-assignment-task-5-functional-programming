package GUI;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GUI01 {

  public static void main(String[] args) {
    JFrame f = new JFrame();
    JButton b = new JButton("Press Me");

    // Set the layout manager to FlowLayout
    f.setLayout(new FlowLayout());

    // Add the button to the frame
    f.add(b);

    b.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent event) {
        System.out.println("Button says: " + 
          event.getActionCommand());
      }
    });

    f.pack();
    f.setLocationRelativeTo(null); // Center the frame on the screen
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.setVisible(true);
  }
}
