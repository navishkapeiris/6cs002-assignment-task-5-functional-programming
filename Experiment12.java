package Experiments;
import java.util.*;
import java.util.function.Function;

public class Experiment12 {
  // Custom Function for manipulating stock entries based on a specific transformation
  class StockNameRemover implements Function<String, String> {
    public String apply(String stockEntry) {
      // Assuming the transformation is removing the currency symbol from the stock price
      String[] parts = stockEntry.split(",");
      String stockName = parts[1].trim().replaceAll("e", ""); // Removing 'e' from the stock name
      return parts[0] + ", " + stockName + ", " + parts[2]; 
    }
  }

  public void run() {
    // Stock market data example
    String[] stockData = {
         "NFLX, Netflix Inc., 600.3, 90000",
        "GOOG, Alphabet Inc., 2800.1, 70000",
        "NVDA, NVIDIA Corp, 750.25, 55000",
        "BABA, Alibaba Group, 220.75, 110000",
        "IBM, IBM Corp, 140.3, 65000",
        "INTC, Intel Corp, 50.75, 95000",
        "AMD, Advanced Micro, 110.8, 60000",
        "INTEL, Intel Corporation, 47.9, 88000",
        "Cisco, Cisco Systems, 60.2, 70000",
        "PFE, Pfizer Inc, 42.75, 95000",
        "MRNA, Moderna Inc, 240.3, 110000"
    };

    List<String> stockList = Arrays.asList(stockData);

    // Transform stock entries based on the custom transformation
    stockList.stream().map(new StockNameRemover())
        .forEach(transformedStockEntry -> System.out.println(transformedStockEntry));
  }

  public static void main(String[] args) {
    new Experiment12().run();
  }
}
