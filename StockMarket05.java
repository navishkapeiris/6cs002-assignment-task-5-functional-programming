package StockMarcket;
import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;
public class StockMarket05 {
    public static void main(String[] args) {
        List<Stock> stockList = Arrays.asList(
                new Stock("AAPL", "Apple Inc.", 150.50, 155.25, 1000000, 154.75, 0.50, 1.0),
                new Stock("GOOGL", "Alphabet Inc.", 1200.75, 1210.50, 800000, 1208.20, 0.75, 1.0)
        );

        System.out.println("Sorted by Comparator in Stock class:");
        stockList.stream().sorted().forEach(System.out::println);

        System.out.println();
        System.out.println("Sorted by lambda (Points Against):");
        stockList.stream()
                .sorted((s1, s2) -> Double.compare(s1.getPointsAgainst(), s2.getPointsAgainst()))
                .forEach(System.out::println);
    }
}
