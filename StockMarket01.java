package StockMarcket;
import java.util.Arrays;
import java.util.List;

public class StockMarket01 {
    public static void main(String[] args) {
        List<Stock> stockList = Arrays.asList(
                new Stock("AAPL", "Apple Inc.", 150.50, 155.25, 1000000, 154.75, 0.50, 1.0),
                new Stock("GOOGL", "Alphabet Inc.", 1200.75, 1210.50, 800000, 1208.20, 0.75, 1.0)
                );

        stockList.forEach(stock -> System.out.println(stock));
    }
}

