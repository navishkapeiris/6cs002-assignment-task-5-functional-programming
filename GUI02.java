package GUI;
import java.awt.*;
import javax.swing.JButton;
import javax.swing.JFrame;

public class GUI02 {

  public static void main(String[] args) {
    JFrame f = new JFrame();
    JButton b = new JButton("Press Me");

    // Use FlowLayout to center the button
    f.setLayout(new FlowLayout(FlowLayout.CENTER));

    // Add the button to the frame
    f.add(b);

    b.addActionListener(
        event -> System.out.println("Button says: " + 
                   event.getActionCommand()));

    f.pack();
    f.setLocationRelativeTo(null); // Center the frame on the screen
    f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    f.setVisible(true);
  }
}
