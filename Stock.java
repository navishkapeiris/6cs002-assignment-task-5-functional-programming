package StockMarcket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Stock implements Comparable<Stock> {
    private String symbol;
    private String companyName;
    private double openPrice;
    private double closePrice;
    private long volume;
    private double adjClosePrice;
    private double dividend;
    private double splitCoefficient;

    public Stock(String symbol, String companyName, double openPrice, double closePrice,
                 long volume, double adjClosePrice, double dividend, double splitCoefficient) {
        this.symbol = symbol;
        this.companyName = companyName;
        this.openPrice = openPrice;
        this.closePrice = closePrice;
        this.volume = volume;
        this.adjClosePrice = adjClosePrice;
        this.dividend = dividend;
        this.splitCoefficient = splitCoefficient;
    }

    public String toString() {
        return String.format("%-5s%-25s%10.2f%10.2f%15d", symbol, companyName, openPrice,
                closePrice, volume);
    }

    // Getters and Setters
    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public double getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(double openPrice) {
        this.openPrice = openPrice;
    }

    public double getClosePrice() {
        return closePrice;
    }

    public void setClosePrice(double closePrice) {
        this.closePrice = closePrice;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public double getAdjClosePrice() {
        return adjClosePrice;
    }

    public void setAdjClosePrice(double adjClosePrice) {
        this.adjClosePrice = adjClosePrice;
    }

    public double getDividend() {
        return dividend;
    }

    public void setDividend(double dividend) {
        this.dividend = dividend;
    }

    public double getSplitCoefficient() {
        return splitCoefficient;
    }

    public void setSplitCoefficient(double splitCoefficient) {
        this.splitCoefficient = splitCoefficient;
    }

    public int compareTo(Stock otherStock) {
        return this.symbol.compareTo(otherStock.symbol);
    }

    public static void main(String[] args) {
        // Creating instances of the Stock class
        Stock stock1 = new Stock("AAPL", "Apple Inc.", 150.25, 155.50, 1000000, 153.0, 2.0, 1.5);
        Stock stock2 = new Stock("GOOGL", "Alphabet Inc.", 2800.50, 2825.75, 500000, 2045.0, 1.0, 2.0);
        Stock stock3 = new Stock("MSFT", "Microsoft Corp.", 310.75, 308.50, 800000, 303.0, 1.5, 1.0);

        // Creating a list to hold the stocks
        List<Stock> stockList = new ArrayList<>();
        stockList.add(stock1);
        stockList.add(stock2);
        stockList.add(stock3);

        // Sorting the stocks based on their symbols
        Collections.sort(stockList);

        // Printing the stocks
        for (Stock stock : stockList) {
            System.out.println(stock.toString());
        }
    }

	public double getPointsAgainst() {
		return 0;
	}
}
