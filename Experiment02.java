package Experiments;
public class Experiment02 {
	  public static void main(String[] args) {
	    // Stock market data example
	    String[] stockData = {
	        "AAPL, Apple Inc., $150.25",
	        "GOOGL, Alphabet Inc., $2800.50",
	        "MSFT, Microsoft Corporation, $310.75"
	    };

	    System.out.println("Stock Market Data:");

	    for (String stock : stockData) {
	      System.out.println(stock);
	    }
	  }
	}
